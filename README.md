# Vanilla backgrounds

Collection of Light and Dark backgrounds for Vanilla OS provided by [@kra-mo](https://github.com/kra-mo)

| Light | Dark |
| ----- | ---- |
|![Light](backgrounds/vanilla-default.webp) |![Dark](backgrounds/vanilla-dark.webp) |
